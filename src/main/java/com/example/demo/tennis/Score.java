package com.example.demo.tennis;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public enum Score {
    LOVE("0"),
    FIFTEEN("15"),
    THIRTY("30"),
    FORTY("40"),
    DEUCE("Deuce"),
    ADVANTAGE("Advantage"),
    WIN("Win");
    
    private final String value;
}
