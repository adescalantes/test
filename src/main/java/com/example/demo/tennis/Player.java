package com.example.demo.tennis;

import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class Player {
    
    private String name;
    private Score score;

    public Player(String name) {
        this.name = name;
        this.score = Score.LOVE;
    }

    @Override
    public String toString() {
        return "Player [" +
                "name=" + name +
                ", score=" + score + " " + score.getValue() + 
                "]";
    }
}
