package com.example.demo.tennis;

import lombok.extern.slf4j.Slf4j;

import static com.example.demo.tennis.Score.*;

@Slf4j
public class TennisGame {
    private Player player1;
    private Player player2;

    public TennisGame(Player player1, Player player2) {
        this.player1 = player1;
        this.player2 = player2;
    }

    public void setPoint(Player player) {
        switch (player.getScore()) {
            case LOVE -> player.setScore(FIFTEEN);
            case FIFTEEN -> player.setScore(THIRTY);
            case THIRTY -> player.setScore(FORTY);
            case FORTY -> handleForty(player);
            case DEUCE -> handleDeuce(player);
            case ADVANTAGE -> player.setScore(WIN);
            default -> throw new IllegalStateException("Unexpected value: " + player.getScore());
        }

        getScore();
    }

    private void handleForty(Player player) {
        if ((player1.getScore().equals(FORTY) && player2.getScore().equals(FORTY)) ||
                (player1.getScore().equals(ADVANTAGE) || player2.getScore().equals(ADVANTAGE))) {
            setDeuce();
        } else {
            player.setScore(WIN);
        }
    }

    private void setDeuce() {
        player1.setScore(DEUCE);
        player2.setScore(DEUCE);
    }

    private void handleDeuce(Player player) {
        if (player.equals(player1)) {
            player1.setScore(ADVANTAGE);
            player2.setScore(FORTY);
        } else if (player.equals(player2)) {
            player2.setScore(ADVANTAGE);
            player1.setScore(FORTY);
        }
    }

    public void getScore() {
        log.info(player1 + " " + player2);

        if (player1.getScore().equals(WIN) || player2.getScore().equals(WIN)) {
            resetScore();
        }
    }

    public void resetScore() {
        player1.setScore(LOVE);
        player2.setScore(LOVE);
    }
}
