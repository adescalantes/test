package com.example.demo;

import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;

@RestController
@RequiredArgsConstructor
public class DemoController {
    
    @Value("${spring.application.version}")
    private String appName;
    @GetMapping("/")
    public String index(HttpServletRequest request) {
        return "hello2";
    }
}
