package com.example.demo.tableau;

import lombok.Getter;
import lombok.Setter;
import org.apache.tomcat.jdbc.pool.PoolProperties;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

import java.util.HashMap;
import java.util.Map;

@Component
@ConfigurationProperties(prefix = "tableau")
@Getter
@Setter
public class TableauConfig {

    private final Map<String, String> instancias = new HashMap<>();
    
    private PoolProperties poolProperties = new PoolProperties();
}


