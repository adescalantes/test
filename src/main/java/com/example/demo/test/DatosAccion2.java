package com.example.demo.test;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
public class DatosAccion2 {
    
    private final TipoAccionContratoFabricante tipo;
}
