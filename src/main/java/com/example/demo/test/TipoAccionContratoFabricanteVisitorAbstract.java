package com.example.demo.test;

public abstract class TipoAccionContratoFabricanteVisitorAbstract<T> {
    public T alta() {
        throw new UnsupportedOperationException("Método 'alta' no implementado");
    }

    public T modificacion() {
        throw new UnsupportedOperationException("Método 'modificacion' no implementado");
    }

    public T terminacionInmediata() {
        throw new UnsupportedOperationException("Método 'terminacionInmediata' no implementado");
    }

    public T colocarFechaTerminacion() {
        throw new UnsupportedOperationException("Método 'colocarFechaTerminacion' no implementado");
    }

    public T quitarFechaTerminacion() {
        throw new UnsupportedOperationException("Método 'quitarFechaTerminacion' no implementado");
    }

    public T deshacerTerminacion() {
        throw new UnsupportedOperationException("Método 'deshacerTerminacion' no implementado");
    }

    public T inicioAutomatico() {
        throw new UnsupportedOperationException("Método 'inicioAutomatico' no implementado");
    }

    public T vencimientoAutomatico() {
        throw new UnsupportedOperationException("Método 'vencimientoAutomatico' no implementado");
    }

    public T terminacionAutomatica() {
        throw new UnsupportedOperationException("Método 'terminacionAutomatica' no implementado");
    }
}
