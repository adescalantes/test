package com.example.demo.test;

import lombok.extern.slf4j.Slf4j;

@Slf4j
public class main {

    public static void main(String[] args) {
        DatosAccion2 datosAccion = new DatosAccionFabricante("apellido", "nombre");
        String a = getDatosAccionFabricante(datosAccion);
        log.info(a);
    }

    public static String getDatosAccionFabricante(DatosAccion2 datosAccion) {
        return datosAccion.getTipo().accept(new TipoAccionContratoFabricanteVisitorAbstract<>() {
            @Override
            public String alta() {
                DatosAccionFabricante datosAccion1 = (DatosAccionFabricante) datosAccion;
                return datosAccion1.getNombre();
            }
        });
    }
}

