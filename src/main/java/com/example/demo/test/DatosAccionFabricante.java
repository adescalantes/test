package com.example.demo.test;

import lombok.*;

@Getter
@Setter
@EqualsAndHashCode(callSuper = false)
@ToString
public class DatosAccionFabricante extends DatosAccion2 {
    
    private final String apellido;
    private final String nombre;

    public DatosAccionFabricante(String apellido, String nombre) {
        super(TipoAccionContratoFabricante.ALTA);
        this.apellido = apellido;
        this.nombre = nombre;
    }
}
