package com.example.demo.test;

public enum TipoAccionContratoFabricante {
    ALTA {

        @Override
        public <T> T accept(TipoAccionContratoFabricanteVisitorAbstract<T> visitor) {
            return visitor.alta();
        }
    },
    MODIFICACION {

        @Override
        public <T> T accept(TipoAccionContratoFabricanteVisitorAbstract<T> visitor) {
            return null;
        }
    },
    TERMINACION_INMEDIATA {

        @Override
        public <T> T accept(TipoAccionContratoFabricanteVisitorAbstract<T> visitor) {
            return null;
        }
    },
    COLOCAR_FECHA_TERMINACION {
        @Override
        public <T> T accept(TipoAccionContratoFabricanteVisitorAbstract<T> visitor) {
            return null;
        }
    },
    QUITAR_FECHA_TERMINACION {

        @Override
        public <T> T accept(TipoAccionContratoFabricanteVisitorAbstract<T> visitor) {
            return null;
        }
    },
    DESHACER_TERMINACION {

        @Override
        public <T> T accept(TipoAccionContratoFabricanteVisitorAbstract<T> visitor) {
            return visitor.deshacerTerminacion();
        }
    },
    INICIO_AUTOMATICO {

        @Override
        public <T> T accept(TipoAccionContratoFabricanteVisitorAbstract<T> visitor) {
            return visitor.inicioAutomatico();
        }
    },
    VENCIMIENTO_AUTOMATICO {

        @Override
        public <T> T accept(TipoAccionContratoFabricanteVisitorAbstract<T> visitor) {
            return visitor.vencimientoAutomatico();
        }
    },
    TERMINACION_AUTOMATICA {

        @Override
        public <T> T accept(TipoAccionContratoFabricanteVisitorAbstract<T> visitor) {
            return visitor.terminacionAutomatica();
        }
    };
    public abstract <T> T accept(TipoAccionContratoFabricanteVisitorAbstract<T> visitor);
}
