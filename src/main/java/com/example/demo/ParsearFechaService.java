package com.example.demo;

import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;

import java.sql.Timestamp;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeParseException;
import java.util.Date;
import java.util.Objects;

/**
 * Clase para parsear fechas en diferentes formatos.
 */
@Service
public class ParsearFechaService {
    /** Formato de fecha por defecto utilizado en los constructores. */
    private static final String FORMATO_FECHA_DEFAULT = "yyyy-MM-dd";
    /** Formato de fecha utilizado para parsear con SimpleDateFormat. */
    private final SimpleDateFormat simpleDateFormat;

    /** Formato de fecha utilizado para parsear con DateTimeFormatter. */
    private final DateTimeFormatter dateTimeFormatter;

    /**
     * Constructor por defecto. Utiliza el formato de fecha por defecto "yyyy-MM-dd".
     */
    public ParsearFechaService() {
        this.simpleDateFormat = new SimpleDateFormat(FORMATO_FECHA_DEFAULT);
        this.dateTimeFormatter = DateTimeFormatter.ofPattern(FORMATO_FECHA_DEFAULT);
    }

    /**
     * Constructor que permite especificar el formato de fecha deseado.
     *
     * @param formatoFecha Formato de fecha que se utilizará para parsear las fechas.
     */
    public ParsearFechaService(String formatoFecha) {
        this.simpleDateFormat = new SimpleDateFormat(formatoFecha);
        this.dateTimeFormatter = DateTimeFormatter.ofPattern(formatoFecha);
    }

    public Date stringToDate(String fecha) throws ParseException {
        if (Objects.isNull(fecha)) return null;

        return simpleDateFormat.parse(fecha);
    }

    public String dateToString(Date fecha) {
        if (Objects.isNull(fecha)) return null;

        return simpleDateFormat.format(fecha);
    }

    /**
     * Convertir una fecha de String a Timestamp
     * @param fecha
     * @return Fecha parseada a Timestamp o null.
     */
    public Timestamp stringToTimestamp(String fecha) {
        if (StringUtils.isEmpty(fecha)) return null;
        try {
            return new Timestamp(simpleDateFormat.parse(fecha).getTime());
        } catch (ParseException e) {
            return null;
        }
    }

    /**
     * Convertir una fecha de String a LocalDate
     * @param fecha
     * @return Fecha parseada a LocalDate o null.
     */
    public LocalDate stringToLocalDate(String fecha) {
        if (StringUtils.isEmpty(fecha)) return null;
        try {
            return LocalDate.parse(fecha, dateTimeFormatter);
        } catch (DateTimeParseException e) {
            return null;
        }
    }
}
